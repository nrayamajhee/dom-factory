use crate::{
    document::get_el,
    element::{add_class, el_as, remove_class},
    event::{add_event, get_target},
};
use std::rc::Rc;
use wasm_bindgen::JsCast;
use web_sys::{
    DomStringMap, DragEvent, Element, Event, File, FileList, FileReader, HtmlElement,
    HtmlInputElement, HtmlTextAreaElement, ProgressEvent,
};

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Validity<'a> {
    Invalid(&'a str),
    Valid,
}

pub fn set_validity(el: &HtmlInputElement, validity: Validity) {
    let message = if let Validity::Invalid(msg) = validity {
        msg
    } else {
        "Invalid Input!"
    };
    if validity == Validity::Valid {
        remove_class(&el, "invalid");
    } else {
        el.set_custom_validity(message);
        add_class(&el, "invalid");
    }
}

pub fn dataset(el: Element) -> (HtmlElement, DomStringMap) {
    let hel = el_as::<HtmlElement>(el);
    let datset = hel.dataset();
    (hel, datset)
}

pub fn mark_valid(el: &HtmlInputElement) {
    set_validity(el, Validity::Valid);
}

pub fn mark_invalid(el: &HtmlInputElement) {
    set_validity(el, Validity::Invalid("Invalid input!"));
}

pub fn input_el_from(el: Element) -> HtmlInputElement {
    el.dyn_into::<HtmlInputElement>()
        .unwrap_or_else(|e| panic!("This is not an inpute element!\n{:?}", e))
}

pub fn get_value(id: &str) -> String {
    input_el_from(get_el(id)).value()
}

pub fn get_files(id: &str) -> FileList {
    input_el_from(get_el(id))
        .files()
        .expect("No files in the input element")
}

pub fn get_target_file_result(e: &Event) -> String {
    get_target(&e)
        .dyn_into::<FileReader>()
        .unwrap_or_else(|e| panic!("Can't cast as File Reader!\n{:?}", e))
        .result()
        .unwrap_or_else(|e| panic!("File reader has no result content!\n{:?}", e))
        .as_string()
        .expect("Can't parse reader result as string!")
}

pub fn get_progress(e: Event) -> ProgressEvent {
    e.dyn_into::<ProgressEvent>()
        .expect("Can't cast event as ProgrssEvent")
}

pub fn textarea_el_from(el: Element) -> HtmlTextAreaElement {
    el.dyn_into::<HtmlTextAreaElement>()
        .unwrap_or_else(|e| panic!("This is not an inpute element!\n{:?}", e))
}

pub fn get_dragged_file(e: Event) -> File {
    let data_items = e
        .dyn_into::<DragEvent>()
        .expect("Can't cast as DragEvent!")
        .data_transfer()
        .unwrap()
        .items();
    if data_items.length() > 0 {
        data_items.get(0).unwrap().get_as_file().unwrap().unwrap()
    } else {
        panic!("This drag event's data transfer items had no items!");
    }
}

pub fn drag_n_drop_then<F>(el: Element, then: F)
where
    F: 'static + Fn(File),
{
    let element = Rc::new(el);
    let el = element.clone();
    add_event(&element, "dragenter", move |_| {
        add_class(&el, "dragover");
    });
    let el = element.clone();
    add_event(&element, "dragleave", move |_| {
        remove_class(&el, "dragover");
    });
    add_event(&element, "dragover", move |e| {
        e.prevent_default();
    });
    let el = element.clone();
    add_event(&element, "drop", move |e| {
        remove_class(&el, "dragover");
        e.prevent_default();
        let file = get_dragged_file(e);
        crate::log!(file);
        then(file);
    });
}
