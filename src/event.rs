use crate::{
    document::{now, window},
    element::el_as,
};
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{Element, Event, EventTarget, HtmlInputElement, KeyboardEvent, MouseEvent};

use js_sys::Promise;
use wasm_bindgen_futures::JsFuture;

pub async fn promise_resolve(promise: &Promise) -> Result<JsValue, JsValue> {
    JsFuture::from(Promise::resolve(promise)).await
}

pub fn get_target(e: &Event) -> EventTarget {
    e.target().expect("No target element for the event!")
}

pub fn get_target_el(e: &Event) -> Element {
    get_target(e)
        .dyn_into::<Element>()
        .expect("Can't cast as Element!")
}

pub fn get_target_value<V: std::str::FromStr>(e: &Event) -> V {
    let value = el_as::<HtmlInputElement>(get_target_el(e)).value();
    value.parse().unwrap_or_else(|_| {
        panic!(
            "Couldn't parse {} as the given value as {}!",
            value,
            stringify!(V)
        )
    })
}

pub fn get_key_event(e: Event) -> KeyboardEvent {
    e.dyn_into::<KeyboardEvent>()
        .expect("This event is not a keyboard event!")
}

pub fn get_mouse_event(e: Event) -> MouseEvent {
    e.dyn_into::<MouseEvent>()
        .expect("This is not a mouse event!")
}

pub fn add_event(
    target: &EventTarget,
    event_type: &str,
    event_listener: impl FnMut(Event) + 'static,
) -> Closure<dyn FnMut(Event)> {
    let cl = Closure::wrap(Box::new(event_listener) as Box<dyn FnMut(_)>);
    target
        .add_event_listener_with_callback(event_type, cl.as_ref().unchecked_ref())
        .unwrap();
    cl
}

pub fn add_event_and_forget(
    target: &EventTarget,
    event_type: &str,
    event_listener: impl FnMut(Event) + 'static,
) {
    let cl = add_event(target, event_type, event_listener);
    cl.forget();
}

pub fn remove_event<H>(target: &EventTarget, event_type: &str, closure: Closure<dyn FnMut(Event)>) {
    target
        .remove_event_listener_with_callback(event_type, closure.as_ref().unchecked_ref())
        .unwrap();
}

pub fn set_interval(callback: impl Fn() + 'static, timeout: i32) -> i32 {
    let cl = Closure::wrap(Box::new(callback) as Box<dyn Fn()>);
    let id = window()
        .set_interval_with_callback_and_timeout_and_arguments_0(
            cl.as_ref().unchecked_ref(),
            timeout,
        )
        .unwrap();
    cl.forget();
    id
}

use std::future::Future;

pub fn set_timeout(callback: impl Fn() + 'static, timeout: i32) -> i32 {
    let cl = Closure::wrap(Box::new(callback) as Box<dyn Fn()>);
    let id = window()
        .set_timeout_with_callback_and_timeout_and_arguments_0(cl.as_ref().unchecked_ref(), timeout)
        .unwrap();
    cl.forget();
    id
}

pub fn request_animation_frame(f: &Closure<dyn FnMut()>) -> i32 {
    window()
        .request_animation_frame(f.as_ref().unchecked_ref())
        .expect("should register `requestAnimationFrame` OK")
}

pub fn cancel_animation_frame(id: i32) {
    window()
        .cancel_animation_frame(id)
        .unwrap_or_else(|_| panic!("Can't cancel animation frame with id: {}", id));
}

use std::cell::RefCell;
use std::rc::Rc;

pub fn on_animation_frame(mut closure: impl FnMut(f64) + 'static, fps: Option<f64>) {
    let t = Rc::new(RefCell::new(0.));
    let f = Rc::new(RefCell::new(None));
    let g = f.clone();
    let then = t.clone();
    let closure = Closure::wrap(Box::new(move || {
        let mut then = then.borrow_mut();
        let delta = now() - *then;
        closure(delta);
        *then = now();
        let h = f.clone();
        let next_frame = move || {
            request_animation_frame(h.borrow().as_ref().unwrap());
        };
        if let Some(fps) = fps {
            set_timeout(next_frame, ((1000. / fps) - delta) as i32);
        } else {
            next_frame();
        };
    }) as Box<dyn FnMut()>);
    *g.borrow_mut() = Some(closure);
    *t.borrow_mut() = now();
    request_animation_frame(g.borrow().as_ref().unwrap());
}
