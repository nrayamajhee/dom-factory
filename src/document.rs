use crate::element::create_el;
use wasm_bindgen::{JsCast, JsValue};
use web_sys::{
    Document, Element, HtmlCanvasElement, HtmlCollection, HtmlElement, HtmlHeadElement,
    HtmlStyleElement, NodeList, Window,
};

pub fn window() -> Window {
    web_sys::window().expect("No global window found!")
}

pub fn document() -> Document {
    window().document().expect("Window has no document!")
}

pub fn body() -> HtmlElement {
    document().body().expect("Document has no body!")
}

pub fn head() -> HtmlHeadElement {
    document().head().unwrap()
}

pub fn push_history(title: &str) -> Result<(), JsValue> {
    window().history().unwrap().push_state_with_url(
        &JsValue::from_str(title),
        "",
        Some(&format!("/{}", title)),
    )
}

pub fn replace_history(title: &str) -> Result<(), JsValue> {
    window().history().unwrap().replace_state_with_url(
        &JsValue::from_str(title),
        "",
        Some(&format!("/{}", title)),
    )
}

pub fn add_style(style: &str) {
    let style_el = create_el("style").dyn_into::<HtmlStyleElement>().unwrap();
    style_el.set_type("text/css");
    style_el.set_inner_html(style);
    head().append_child(&style_el).unwrap();
}

pub fn try_get_el(id: &str) -> Option<Element> {
    document().get_element_by_id(id)
}

pub fn get_el(id: &str) -> Element {
    try_get_el(id).unwrap_or_else(|| panic!("Element with id {} not found in document!", id))
}

pub fn try_get_els_by_class(class: &str) -> HtmlCollection {
    document().get_elements_by_class_name(class)
}

pub fn get_els_by_class(class: &str) -> Element {
    try_get_els_by_class(class)
        .item(0)
        .unwrap_or_else(|| panic!("Cant find element with class {}", class))
}

pub fn try_query_el(selector: &str) -> Option<Element> {
    document().query_selector(selector).unwrap_or_else(|err| {
        panic!(
            "There was an error running query selector: {}\n{:?}",
            selector, err
        )
    })
}

pub fn query_el(selector: &str) -> Element {
    try_query_el(selector).unwrap_or_else(|| panic!("No element matches selector: {}", selector))
}

pub fn query_els(selector: &str) -> NodeList {
    document()
        .query_selector_all(selector)
        .unwrap_or_else(|e| panic!("No element matches selector: {}!\n{:?}", selector, e))
}

pub fn for_each_in(list: NodeList, closure: impl Fn(Element)) {
    for i in 0..list.length() {
        let item = list.get(i).unwrap();
        closure(item.dyn_into::<Element>().unwrap());
    }
}

pub fn now() -> f64 {
    window()
        .performance()
        .expect("Performance should be available")
        .now()
}

pub fn fullscree_canvas(canvas: &HtmlCanvasElement, pixel_ratio: f64) -> f64 {
    let window = window();
    let pixel_ratio = window.device_pixel_ratio() * pixel_ratio;
    let width: u32 = (window.inner_width().unwrap().as_f64().unwrap() * pixel_ratio) as u32;
    let height: u32 = (window.inner_height().unwrap().as_f64().unwrap() * pixel_ratio) as u32;
    canvas.set_width(width);
    canvas.set_height(height);
    width as f64 / height as f64
}

pub fn get_canvas(id: &str) -> HtmlCanvasElement {
    get_el(id)
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .expect("Can't convert the dom element to HtmlCanvasElement!")
}
