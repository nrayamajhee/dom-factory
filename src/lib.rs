use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    pub fn log(s: &str);
    #[wasm_bindgen(js_namespace = console, js_name = log)]
    pub fn logv(x: &JsValue);
}

#[macro_export]
macro_rules! console_log {
    ($($t:tt)*) => {
        use wasm_bindgen::prelude::*;
        #[wasm_bindgen]
        extern "C" {
            #[wasm_bindgen(js_namespace = console)]
            pub fn log(s: &str);
        }
        (log(&format_args!($($t)*).to_string()))
    }
}

#[macro_export]
macro_rules! log {
    ($($each:expr),*$(,)?) => {
        {
            let mut msg = String::new();
            $(
                let any = &$each as &dyn std::any::Any;
                if let Some(val) = any.downcast_ref::<&str>() {
                    msg = format!("{} {}", msg, val);
                } else if let Some(val) = any.downcast_ref::<String>() {
                    msg = format!("{} {}", msg, val);
                } else {
                    msg = format!("{} {:?}", msg, $each);
                }
            )*
            use wasm_bindgen::prelude::*;
            #[wasm_bindgen]
            extern "C" {
                #[wasm_bindgen(js_namespace = console)]
                pub fn log(s: &str);
            }
            log(&msg);
        }
    }
}

#[macro_export]
macro_rules! pub_use_mod {
    ($($mod:ident),+$(,)?) => {
        $(
            mod $mod;
            pub use $mod::*;
        )*
    }
}

#[macro_export]
macro_rules! pub_mod {
    ($($mod:ident),+$(,)?) => {
        $(
            pub mod $mod;
        )*
    }
}

use std::cell::RefCell;
use std::ops::Deref;
use std::rc::Rc;

#[derive(Debug)]
pub struct RcCell<T>(pub Rc<RefCell<T>>);

impl<T> RcCell<T> {
    pub fn new(inner: T) -> Self {
        Self(Rc::new(RefCell::new(inner)))
    }
    pub fn mutate(&self, value: T) {
        *self.0.borrow_mut() = value;
    }
}

impl<T> Clone for RcCell<T> {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

impl<T> Deref for RcCell<T> {
    type Target = RefCell<T>;

    fn deref(&self) -> &Self::Target {
        &*self.0
    }
}

use std::sync::{Arc, Mutex, MutexGuard};

#[derive(Debug)]
pub struct ArcMut<T>(pub Arc<Mutex<T>>);

unsafe impl<T> Send for ArcMut<T> {}

unsafe impl<T> Sync for ArcMut<T> {}

impl<T> ArcMut<T> {
    pub fn new(inner: T) -> Self {
        Self(Arc::new(Mutex::new(inner)))
    }
    pub fn mutate(&self, value: T) {
        *self.0.lock().unwrap() = value;
    }
    pub fn unlock(&self) -> MutexGuard<T> {
        self.0.lock().unwrap()
    }
}

impl<T> Clone for ArcMut<T> {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

impl<T> Deref for ArcMut<T> {
    type Target = Mutex<T>;

    fn deref(&self) -> &Self::Target {
        &*self.0
    }
}

pub_mod!(document, element, shadow, form, event, fetch, navigator);
