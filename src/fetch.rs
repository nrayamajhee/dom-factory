use crate::document::window;
use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen_futures::{future_to_promise, JsFuture};
use web_sys::{Request, RequestInit, RequestMode, Response};
#[derive(PartialEq, Debug, Clone)]
pub enum FetchMethod {
    Get,
    Post(JsValue),
}

impl FetchMethod {
    pub fn post<S: Serialize>(body: &S) -> Self {
        FetchMethod::Post(serde_json::to_string(body).unwrap().into())
    }
}

pub async fn fetch_json(url: &str, method: FetchMethod) -> Result<JsValue, JsValue> {
    let mut opts = RequestInit::new();
    if let FetchMethod::Post(body) = method {
        opts.method("POST");
        opts.body(Some(&body));
    } else {
        opts.method("GET");
    }
    let request = Request::new_with_str_and_init(&url, &opts)?;
    request.headers().set("Content-Type", "application/json")?;
    let resp_value = JsFuture::from(window().fetch_with_request(&request)).await?;
    assert!(resp_value.is_instance_of::<Response>());
    let resp: Response = resp_value.dyn_into()?;
    let json = JsFuture::from(resp.json()?).await?;
    Ok(json)
}

pub async fn try_fetch_as<T>(url: &str, method: FetchMethod) -> Result<T, serde_json::Error>
where
    T: for<'a> Deserialize<'a>,
{
    let json = fetch_json(url, method)
        .await
        .unwrap_or_else(|err| panic!("Couldn't fetch response:\n{:?}", err));
    json.into_serde::<T>()
}

pub async fn fetch_as<T>(url: &str, method: FetchMethod) -> T
where
    T: for<'a> Deserialize<'a>,
{
    let json = fetch_json(url, method)
        .await
        .unwrap_or_else(|err| panic!("Couldn't fetch response:\n{:?}", err));
    json.into_serde::<T>()
        .expect("Couldn't serialize API json into response!")
}

pub fn try_fetch_then<F: 'static + Fn(JsValue)>(
    url: &'static str,
    method: FetchMethod,
    closure: F,
) {
    let resolve =
        Closure::wrap(Box::new(move |json: JsValue| closure(json)) as Box<dyn FnMut(JsValue)>);
    let m = method.clone();
    let reject = Closure::wrap(Box::new(move |err| {
        crate::log!("Failed to", m, "data!\n", err);
    }) as Box<dyn FnMut(JsValue)>);
    let _ = future_to_promise(fetch_json(&url, method))
        .then(&resolve)
        .catch(&reject);
    resolve.forget();
    reject.forget();
}

pub fn fetch_then<T, F, S>(url: &'static str, method: FetchMethod, closure: F)
where
    T: for<'a> Deserialize<'a>,
    F: 'static + Fn(T),
{
    try_fetch_then(url, method, move |json: JsValue| {
        closure(json.into_serde::<T>().unwrap_or_else(|err| {
            panic!(
                "Couldn't deserialize response into the given type!\n{:?}",
                err
            )
        }));
    });
}
