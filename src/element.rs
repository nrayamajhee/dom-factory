use wasm_bindgen::JsCast;
use web_sys::{Element, HtmlElement};

use crate::document::document;

pub fn el_as<T: JsCast>(el: web_sys::Element) -> T {
    el.dyn_into::<T>()
        .unwrap_or_else(|e| panic!("Cannot cast as {}!\n{:?}", stringify!(T), e))
}

pub fn create_el(name: &str) -> Element {
    document()
        .create_element(name)
        .unwrap_or_else(|e| panic!("Can't create element with name {}!\n{:?}", name, e))
}

pub fn el_from(html_el: HtmlElement) -> Element {
    html_el
        .dyn_into::<Element>()
        .unwrap_or_else(|e| panic!("Cannot cast as element!\n{:?}", e))
}

pub fn add_class(el: &Element, class_name: &str) {
    el.class_list().add_1(class_name).unwrap_or_else(|e| {
        panic!(
            "Can't add class name: {} to element: {:?}!\n{:?}",
            class_name, el, e
        )
    });
}

pub fn remove_class(el: &Element, class_name: &str) {
    el.class_list().remove_1(class_name).unwrap_or_else(|e| {
        panic!(
            "Can't add class name: {} to element: {:?}!\n{:?}",
            class_name, el, e
        )
    });
}

pub fn create_el_w_id(name: &str, id: &str) -> Element {
    let el = create_el(name);
    el.set_attribute("id", id)
        .unwrap_or_else(|e| panic!("Can't set attribute {} for {}!\n{:?}", name, id, e));
    el
}

pub fn create_el_w_class_n_inner(name: &str, class: &str, inner_html: &str) -> Element {
    let el = create_el(name);
    for each in class.split(' ') {
        add_class(&el, each);
    }
    el.set_inner_html(inner_html);
    el
}

pub fn create_el_w_attrs(name: &str, attributes: &[(&str, &str)]) -> Element {
    let el = create_el(name);
    for (key, value) in attributes.iter() {
        el.set_attribute(key, value)
            .unwrap_or_else(|e| panic!("Can't set attribute {} for {}!\n{:?}", key, name, e));
    }
    el
}

#[derive(Debug)]
pub enum InsertOption {
    Before,
    Start,
    End,
    After,
}

impl InsertOption {
    fn dom_string(&self) -> &'static str {
        match self {
            Self::Before => "beforebegin",
            Self::Start => "afterbegin",
            Self::End => "beforeend",
            Self::After => "afterend",
        }
    }
}

pub fn insert_html_at(parent: &Element, inner: &str, option: InsertOption) {
    parent
        .insert_adjacent_html(option.dom_string(), &inner)
        .unwrap_or_else(|e| panic!("Can't insert element {:?}!\n{:?}", option, e));
}

pub fn insert_el_at(parent: &Element, child: &Element, option: InsertOption) {
    parent
        .insert_adjacent_element(option.dom_string(), &child)
        .unwrap_or_else(|e| panic!("Can't insert element {:?}!\n{:?}", option, e));
}

pub fn insert_el(parent: &Element, child: &Element) {
    insert_el_at(parent, child, InsertOption::End);
}

pub fn insert_html(parent: &Element, inner: &str) {
    insert_html_at(parent, inner, InsertOption::End);
}

pub fn get_nth_child(el: &Element, index: u32) -> Element {
    if let Some(el) = el.children().get_with_index(index) {
        el
    } else {
        panic!("This element {} has no children", el.tag_name())
    }
}

pub fn get_parent(node: &Element, level: usize) -> Option<Element> {
    if level == 0 {
        return Some(node.clone());
    }
    if let Some(parent) = node.parent_element() {
        get_parent(&parent, level - 1)
    } else {
        None
    }
}
