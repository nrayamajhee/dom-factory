use crate::{
    document::{for_each_in, query_els, window},
    element::{create_el, create_el_w_attrs, create_el_w_id},
    event::add_event,
    RcCell,
};
use maud::Markup;
use std::collections::HashMap;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{
    Element, Event, HtmlStyleElement, NodeList, ShadowRoot, ShadowRootInit, ShadowRootMode,
};

pub fn get_el_in_shadow(shadow: &ShadowRoot, id: &str) -> Element {
    shadow
        .get_element_by_id(id)
        .unwrap_or_else(|| panic!("No element with id {} found", id))
}

pub fn query_els_in_shadow(shadow: &ShadowRoot, selector: &str) -> NodeList {
    shadow
        .query_selector_all(selector)
        .unwrap_or_else(|e| panic!("No element matches selector: {}\n{:?}", selector, e))
}

pub fn try_query_el_in_shadow(shadow: &ShadowRoot, selector: &str) -> Option<Element> {
    shadow
        .query_selector(selector)
        .unwrap_or_else(|e| panic!("Cant find element with selector {}:\n{:?}", selector, e))
}

pub fn query_el_in_shadow(shadow: &ShadowRoot, selector: &str) -> Element {
    try_query_el_in_shadow(shadow, selector)
        .unwrap_or_else(|| panic!("No element matches selector: {}", selector))
}

pub enum Style {
    Link(String),
    Text(String),
    None,
}

pub fn create_root(name: &str, markup: &str, style: Style) -> Element {
    let name = if name.contains('-') {
        name.into()
    } else {
        format!("component-{}", name)
    };
    let el = create_el(&name);
    let shadow = el
        .attach_shadow(&ShadowRootInit::new(ShadowRootMode::Open))
        .unwrap_or_else(|e| {
            panic!("Can't attach shadow root:\n{:?}", e);
        });
    shadow.set_inner_html(markup);
    let stylesheets = query_els("link[rel=stylesheet]");
    for i in 0..stylesheets.length() {
        let each = stylesheets.get(i).expect("No style element found!");
        let href = each
            .dyn_into::<Element>()
            .unwrap()
            .get_attribute("href")
            .unwrap();
        let style = create_el_w_attrs("link", &[("rel", "stylesheet"), ("href", &href)]);
        shadow
            .append_child(&style)
            .expect("Cant' append stylesheet to shadow root");
    }
    match style {
        Style::Link(href) => {
            let style = create_el_w_attrs("link", &[("rel", "stylesheet"), ("href", &href)]);
            shadow
                .append_child(&style)
                .expect("Cant' append stylesheet to shadow root");
        }
        Style::Text(style) => {
            let style_el = create_el("style").dyn_into::<HtmlStyleElement>().unwrap();
            style_el.set_type("text/css");
            style_el.set_inner_html(&style);
            shadow.append_child(&style_el).unwrap();
        }
        _ => (),
    }
    el
}
