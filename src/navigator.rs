use crate::document::{window};
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

pub fn on_location<F>(closure: F)
where
    F: 'static + FnMut(JsValue),
{
    if let Ok(loc) = window().navigator().geolocation() {
        let clo = Closure::wrap(Box::new(closure) as Box<dyn FnMut(_)>);
        loc.get_current_position(clo.as_ref().unchecked_ref())
            .unwrap();
        clo.forget();
    }
}
